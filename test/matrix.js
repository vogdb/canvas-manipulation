/**
 * Created: vogdb Date: 9/8/13 Time: 2:36 PM
 */

describe('Matrix', function () {
  var m1, m3, m4;

  // Reset each of the matrices before each test and turn silent mode off
  beforeEach(function () {
    m1 = new Matrix([
      [-2, 2, 3],
      [-1, 1, 3],
      [ 2, 0, -1]
    ]);

    m3 = new Matrix([
      [1, 2, 3],
      [4, 5, 6],
      [7, 8, 9]
    ]);

    m4 = new Matrix([
      [9, 8, 7],
      [6, 5, 4],
      [3, 2, 1]
    ]);

  });

  describe('constructor', function () {
    it('should throw an error if the argument passed is not an array', function () {
      expect(function () {
        new Matrix('hi');
      }).to.throw();
    });
  });

  describe('#multiply()', function () {

    it('should throw an error if the matrix to multiply is not a valid Matrix object', function () {
      expect(function () {
        m1.multiply('hi');
      }).to.throw();
    });

    it('should multiply two matrices', function () {
      expect(m1.clone().multiply(m3)._values).to.eql([
        [27, 30, 33],
        [24, 27, 30],
        [-5, -4, -3]
      ]);
    });

  });

  describe('#inverse()', function () {
    it('should invert a matrix', function () {
      // Test against a precomputed inverse
      expect(new Matrix([
        [2, 0, -1],
        [2, 1, 1],
        [3, 4, 4]
      ]).inverse()._values).to.eql([
        [ 0, .8, -.2],
        [ 1, -2.2, .8],
        [-1, 1.6, -.4]
      ]);

      // Generate a Matrix of random numbers
      var data = [];
      for (var i = 0; i < Matrix.rowLen; i++) {
        data[i] = [];
        for (var j = 0; j < Matrix.colLen; j++) {
          data[i][j] = Math.floor(Math.random() * 101);
        }
      }

      // Invert the matrix and multiply it by itself
      var original = new Matrix(data);
      var inverted = original.clone().inverse();
      var product = original.multiply(inverted);

      // Check if it equals the identity matrix
      for (var i = 0; i < Matrix.rowLen; i++) {
        for (var j = 0; j < Matrix.colLen; j++) {
          expect(Math.abs((i === j ? 1 : 0) - product._values[i][j])).to.be.lessThan(.0000001);
        }
      }
    });
  });

});