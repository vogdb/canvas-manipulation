/**
 * Offers restricted functionality of svg for the part of transformations.
 * By svg means "document.createElementNS("http://www.w3.org/2000/svg", 'svg')"
 */
var SvgTransformSub = function () {
  //TODO if svg exists use it else substitute it
}

SvgTransformSub.prototype.createSVGMatrix = function () {
  return new SvgMatrixSub()
}

SvgTransformSub.prototype.createSVGPoint = function () {
  return new SvgPointSub()
}

/**
 * Offers restricted functionality of <a href="http://www.w3.org/TR/SVG11/coords.html#InterfaceSVGMatrix">SVGMatrix</a>
 * @param matrix Matrix @see <a href="https://github.com/angusgibbs/matrix">https://github.com/angusgibbs/matrix</a>
 */
var SvgMatrixSub = function (matrix) {
  if (matrix) {
    this.matrix = matrix
  } else {
    this.matrix = new Matrix([
       [1, 0, 0]
      ,[0, 1, 0]
      ,[0, 0, 1]
    ])
  }
}

SvgMatrixSub.prototype.setTransformFromArray = function (a, b, c, d, e, f) {
  var values = this.matrix._values
  values[0][0] = a
  values[1][0] = b
  values[0][1] = c
  values[1][1] = d
  values[0][2] = e
  values[1][2] = f
}

SvgMatrixSub.prototype.getTransformAsArray = function () {
  var values = this.matrix._values
  return [
     values[0][0]
    ,values[1][0]
    ,values[0][1]
    ,values[1][1]
    ,values[0][2]
    ,values[1][2]
  ]
}

SvgMatrixSub.prototype.translate = function (tx, ty) {
  return this._multiply([
     [1, 0, tx]
    ,[0, 1, ty]
    ,[0, 0, 1]
  ])
}

SvgMatrixSub.prototype.scaleNonUniform = function (sx, sy) {
  return this._multiply([
     [sx, 0, 0]
    ,[0, sy, 0]
    ,[0, 0, 1]
  ])
}

SvgMatrixSub.prototype.rotate = function (a) {
  return this._multiply([
     [Math.cos(a), -Math.sin(a), 0]
    ,[Math.sin(a), Math.cos(a), 0]
    ,[0, 0, 1]
  ])
}

SvgMatrixSub.prototype._multiply = function (matrix) {
  var result = new SvgMatrixSub(this.matrix.clone())
  result.matrix.multiply(new Matrix(matrix))
  return result
}

SvgMatrixSub.prototype.inverse = function () {
  var result = new SvgMatrixSub(this.matrix.clone())
  result.matrix.inverse()
  return result
}


/**
 * Offers restricted functionality of SVGMatrix {@url http://www.w3.org/TR/SVG11/coords.html#InterfaceSVGMatrix}
 */
var SvgPointSub = function () {
  this.x
  this.y
}

SvgPointSub.prototype.matrixTransform = function (matrix) {
  var resultMatrix = matrix.matrix.clone().multiply(new Matrix([
     [this.x]
    ,[this.y]
    ,[1]
  ]))
  var result = new SvgPointSub()
  result.x = resultMatrix._values[0][0]
  result.y = resultMatrix._values[1][0]
  return result
}
