var previousInit = CanvasManipulation.prototype.init

CanvasManipulation.prototype.init = function () {
  previousInit.call(this, arguments)
  this.initTouchListeners()
}

CanvasManipulation.prototype.initTouchListeners = function () {
  var self = this
  var hammer = Hammer(this.getCanvas(), {'prevent_default': true})
  hammer.on('dragstart', function (evt) {
    self.dragStart(self.touchCoord(evt))
  })
  hammer.on('drag', function (evt) {
    self.drag(self.touchCoord(evt))
  })
  hammer.on('dragend', function () {
    self.dragEnd()
  })
  if (!this.isOldAndroid()) {
    hammer.on('pinchin', function (evt) {
      self.zoom(self.touchCoord(evt), -1)
    })
    hammer.on('pinchout', function (evt) {
      self.zoom(self.touchCoord(evt), 1)
    })
    hammer.on('rotate', function (evt) {
      self.rotateHandler(self.touchCoord(evt), evt.gesture.rotation * Math.PI / 180)
    })
  }
  hammer.on('release', function () {
    self.release()
  })
}

CanvasManipulation.prototype.touchCoord = function (evt) {
  var ex = evt.gesture.center.pageX
  var ey = evt.gesture.center.pageY
  return {x: ex, y: ey}
}

CanvasManipulation.prototype.isOldAndroid = function () {
  return navigator.userAgent.toLowerCase().search('android [23]') !== -1
}

CanvasManipulation.prototype.rotateHandler = function (center, radians) {
  //do not start rotation with a small angle.
  if (!this._isRotating && Math.abs(radians) < Math.PI / 18) {
    return
  }
  this._isRotating = true
  //is it a continue of the started rotation
  if (!isNaN(this._rotateStartAngle)) {
    this.rotate(center, radians - this._rotateStartAngle)
    this._rotateStartAngle = radians
  } else {
    this._rotateStartAngle = radians
    this.rotate(center, this._rotateStartAngle)
  }
}

CanvasManipulation.prototype.release = function () {
  this.dragEnd()
  this._isRotating = false
  this._rotateStartAngle = null
}