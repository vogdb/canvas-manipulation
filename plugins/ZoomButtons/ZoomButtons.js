var previousInit = CanvasManipulation.prototype.init
CanvasManipulation.prototype.init = function () {
  previousInit.call(this, arguments)
  this.createZoomButtons()
}

var previousLayout = CanvasManipulation.prototype.layout
CanvasManipulation.prototype.layout = function () {
  previousLayout.call(this, arguments)
  this.layoutZoomButtons()
}

CanvasManipulation.prototype.createZoomButtons = function () {
  var panel = document.createElement('div')
  panel.style.position = 'absolute'
  panel.style.fontWeight = 'bold'
  this.createZoomButton(panel, 'padding-left: 18%', '+', 1)
  this.createZoomButton(panel, 'padding-left: 28%; padding-top: 0; border-top: 0;', '-', -1)
  document.body.appendChild(panel)
  document.body.appendChild(panel)
  this._zoomPanel = panel
}

CanvasManipulation.prototype.createZoomButton = function (panel, customCss, label, zoomValue) {
  var zoomButton = document.createElement('div')
  zoomButton.innerHTML = label
  zoomButton.style.cssText = 'background-color: #fff; border: 2px solid #000; opacity: 0.5; ' + customCss

  var hammer = Hammer(zoomButton, {'prevent_default': true})

  function zooming() {
    self.zoom({x: self.getCanvas().width / 2, y: self.getCanvas().height / 2}, zoomValue)
  }

  var zoomingInterval
  var self = this
  hammer.on('tap', zooming)
  hammer.on('hold', function () {
    zoomingInterval = setInterval(zooming, 500)
  })
  function stopZooming() {
    clearInterval(zoomingInterval)
  }

  hammer.on('release', stopZooming)
  hammer.on('drag', stopZooming)
  panel.appendChild(zoomButton)
}

CanvasManipulation.prototype.layoutZoomButtons = function () {
  var panel = this._zoomPanel
  var width = Math.floor(this.getCanvas().height * 0.1)
  panel.style.width = width + 'px'
  panel.style.left = this.getCanvas().offsetLeft + width + 'px'
  var height = Math.floor(this.getCanvas().height * 0.2)
  panel.style.height = height + 'px'
  panel.style.top = this.getCanvas().offsetTop + width + 'px'
  panel.style.fontSize = width + 'px'
}