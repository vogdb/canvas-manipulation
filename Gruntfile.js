module.exports = function (grunt) {

  var compiler = require('superstartup-closure-compiler')

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json')

    ,banner: '/*!\n <%= pkg.title || pkg.name %> - v<%= pkg.version %> - '
      + '<%= grunt.template.today("yyyy-mm-dd") %>\n'
      + ' <%= (pkg.homepage ? "* " + pkg.homepage : pkg.repository.url) + "\\n" %>'
      + ' Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author %>;'
      + ' Licensed <%= _.pluck(pkg.licenses, "type").join(", ") %>\n*/\n'

    ,concat: {
      options: {
        banner: '<%= banner %>'
      }
      ,dist: {
        src: [
          'src/Matrix.js',
          'src/SvgTransformProxy.js'
          ,'src/TrackTransform.js'
          ,'src/CanvasManipulation.js']
        ,dest: 'dist/canvas-manipulation.js'
      }
    }

    ,closureCompiler: {
      options: {
        compilerFile: compiler.getPath()
        //,checkModified: true
        ,compilerOpts: {
          compilation_level: 'SIMPLE_OPTIMIZATIONS'
          ,warning_level:'QUIET'
          ,output_wrapper: '"(function(){%output%}).call(this);"'
        }
      }
      ,main: {
        TEMPcompilerOpts: {
          compilation_level: 'ADVANCED_OPTIMIZATIONS'
          ,externs: 'src/gcc/externs/*.js'
          ,useGoogExport: true
          ,output_wrapper: '"<%= banner %>;(function(){%output%}).call(this)"'
        }
        ,src: ['dist/canvas-manipulation.js','src/gcc/CanvasManipulationExports.js']
        ,dest: 'dist/canvas-manipulation.min.js'
      }
      ,mouse: {
        src: ['plugins/MouseListeners/*.js']
        ,dest: ['dist/canvas-manipulation-mouse-listeners.min.js']
      }
      ,touch: {
        src: ['plugins/TouchListeners/*.js']
        ,dest: ['dist/canvas-manipulation-touch-listeners.min.js']
      }
      ,zoom: {
        src: ['plugins/ZoomButtons/*.js']
        ,dest: ['dist/canvas-manipulation-zoom-buttons.min.js']
      }
    }

    ,watch: {
      files: ['Gruntfile.js', 'src/**/*', 'index.html']
      ,tasks: 'concat'
    }

    ,mocha: {
      options: {
        bail: true
        ,log: true
        ,mocha: {
          ignoreLeaks: false
        }
        ,run: true
      }
      ,matrix: {
        src: 'test/matrix.html'
      }
    }

    ,jsdoc: {
      dist: {
        src: ['src/CanvasManipulation.js'],
        options: {
          destination: 'doc'
        }
      }
    }
  })

  grunt.loadNpmTasks('grunt-contrib-concat')
  grunt.loadNpmTasks('grunt-contrib-watch')
  grunt.loadNpmTasks('grunt-closure-tools')
  grunt.loadNpmTasks('grunt-mocha')
  grunt.loadNpmTasks('grunt-jsdoc')

  grunt.registerTask('default', ['concat', 'mocha'])

}
