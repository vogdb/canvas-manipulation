# Canvas Manipulation
Library that allows you to manipulate the canvas. Right now the manipulation
includes drag, zoom and rotate. The manipulation can be triggered by user actions
such as mouse click or directly by library API.

## Example usage
Example of the manipulation that triggered by mouse events can be found at examples/mouse.html

Example of the manipulation that triggered by touch events can be found at examples/touch.html

## API
Generate it with the command `grunt jsdoc`. If you can't do this then please read plain
jsdoc from src/CanvasManipulation.js.